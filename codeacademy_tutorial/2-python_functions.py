
def average(num1,num2):
  return (num1+num2)/2

print(average(1, 100))

def tenth_power(num):
  return num ** 10

print(tenth_power(2))

def introduction(first_name, last_name):
  return last_name + ", " + first_name + " " + last_name

print(introduction("James", "Bond"))

def square_root(num):
  return num ** 0.5

print(square_root(16))

def tip(total, percentage):
  return total*(percentage/100)

print(tip(10, 25))

def win_percentage(wins,losses):
  return (wins/(wins+losses))*100

print(win_percentage(5, 5))

def first_three_multiples(num):
  mul1=num*1
  print(mul1)
  mul2=num*1
  print(mul2)
  mul3=num*1
  print(mul3)
  totalmul= mul1+mul2+mul3
  return totalmul

first_three_multiples(10)

#este str es muy útil para castear
def dog_years(name, age):
  return name+", you are "+str(age*7)+" years old in dog years"

print(dog_years("Lola", 16))

def remainder(num1, num2):
  return (2*num1)%(num2/2)

print(remainder(15, 14))

def lots_of_math(a, b, c, d):
  first = a+b
  second = c-d
  third = first*second
  fourth = third%a
  print(first)
  print(second)
  print(third)
  return fourth

print(lots_of_math(1, 2, 3, 4))






