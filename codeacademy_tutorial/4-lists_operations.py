list1 = range(2, 20, 3)
list1_len = len(list1)

#longitud de la lista
print(list1_len)
#imprime el 20
print(list1[1])
#imprime el último elemento, en este caso 3
print(list1[-1])
#imprime el rango entre los elemento, en este caso solo imprimira el 20
middle = suitcase[0:2]
print(middle)
#hasta el 3
start = suitcase[:3]
#a partir del 4
end = suitcase[4:]
print(start, end)

votes = ['Jake', 'Jake', 'Laurie', 'Laurie', 'Laurie', 'Jake', 'Jake', 'Jake', 'Laurie', 'Cassie', 'Cassie', 'Jake', 'Jake', 'Cassie', 'Laurie', 'Cassie', 'Jake', 'Jake', 'Cassie', 'Laurie']
jake_votes = votes.count("Jake")
print(jake_votes)

#WAY1
names = ['Ron', 'Hermione', 'Harry', 'Albus', 'Sirius']
names.sort()
print(names)
#WAY2
games = ['Portal', 'Minecraft', 'Pacman', 'Tetris', 'The Sims', 'Pokemon']
games_sorted = sorted(games)
print(games)
print(games_sorted)

