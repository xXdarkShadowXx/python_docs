#funciones

#si algo no esta indentado se imprime primero por pantalla
def loading_screen():
  print("This page is loading...")
print("This is your desktop")

loading_screen()

def mult_x_add_y(number,x=2,y):
  print(number*x + y)

mult_x_add_y(1,3,1)

def calculate_age(current_year, birth_year):
  age = current_year - birth_year
  return age

my_age = calculate_age(2049,1993)

dads_age = calculate_age(2049,1953)

print("I am ", my_age , "years old and my dad is", dads_age, "years old")

#multiple return

def get_boundaries(target, margin):
  low_limit = target - margin
  high_limit = margin + target

  return low_limit, high_limit

low,high = get_boundaries(100,20)

#global variables
current_year = 2048

def calculate_age(birth_year):
  age = current_year - birth_year
  return age

print(current_year)

print(calculate_age(1970))

#final

def repeat_stuff(stuff, num_repeats=10):
  return stuff*num_repeats

lyrics = repeat_stuff("Row ", 3) + "Your Boat. "

song = repeat_stuff(lyrics)

print(song)
