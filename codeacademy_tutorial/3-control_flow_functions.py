def in_range(num, lower, upper):
  if(num >= lower and num <= upper):
    return True
  return False

print(in_range(10, 10, 10))

def movie_review(rating):
  if(rating <= 5):
    return "Avoid at all costs!"
  if(rating < 9):
    return "This one was fun."
  return "Outstanding!"

print(movie_review(9))

def twice_as_large(num1, num2):
  if num1 > 2 * num2:
    return True
  else:
    return False

print(twice_as_large(10, 5))

def large_power(base, exponent):
  if base ** exponent > 5000:
    return True
  else:
    return False

print(large_power(2, 13))

def divisible_by_ten(num):
  if (num % 10 == 0):
    return True
  else:
    return False

print(divisible_by_ten(20))

# Write your max_num function here:
def max_num(num1, num2, num3):
  if num1 > num2 and num1 > num3:
    return num1
  elif num2 > num1 and num2 > num3:
    return num2
  elif num3 > num1 and num3 > num2:
    return num3
  else:
    return "It's a tie!"

print(max_num(-10, 0, 10))

def over_budget(budget, food_bill, electricity_bill, internet_bill, rent):
  if (budget < food_bill + electricity_bill + internet_bill + rent):
    return True
  else:
    return False

print(over_budget(100, 20, 30, 10, 40))

def always_false(num):
  if (num > 0 and num < 0):
    return True
  else:
    return False

print(always_false(0))

def not_sum_to_ten(num1, num2):
  if (num1 + num2 != 10):
    return True
  else:
    return False

print(not_sum_to_ten(9, -1))

def same_name(your_name, my_name):
  if (your_name == my_name):
    return True
  else:
    return False

print(same_name("Colby", "Colby"))








