my_name , my_name2 = "Pablo" , 'Python'
#imprimir un mensaje
print("Hello and welcome " + my_name + "!")
#es lo mismo comillas dobles que simples
print('This is a tutorial of ' + my_name2 + '!')

string1 = "The wind, "
string2 = "which had hitherto carried us along with amazing rapidity, "
string3 = "sank at sunset to a light breeze; "
string4 = "the soft air just ruffled the water and "
string5 = "caused a pleasant motion among the trees as we approached the shore, "
string6 = "from which it wafted the most delightful scent of flowers and hay."

# Define message below:

message = string1 + string2 + string3 + string4 + string5 + string6

print(message)

#multiline string
to_you = """
Stranger, if you passing meet me and desire to speak to me, why
  should you not speak to me?
And why should I not speak to you?
"""

print(to_you)

rating_out_of_10 = 10.0
#operations
a = 25
b = 68
c = 13
d = 28
print((a*b)+(c/d))
#8 al cuadrado
print(8 ** 2)
#8 elevado a la cuarta
print(8 ** 4)
#el resto
my_team = (27 % 4)
print(my_team)
#sumar y asignar valor
total_price = 0

new_sneakers = 50.00

total_price += new_sneakers

nice_sweater = 39.00
fun_books = 20.00
# Update total_price here:
total_price += nice_sweater + fun_books

print("The total price is", total_price)